/* 
 * main.js - sets up the main configuration settings for require.js
 * 
 * @author Justin Hogg
 */
requirejs.config({
	baseUrl: 'app', //set the base url
	paths: {
		js:			'assets/js',
		views:		'protected/views',
		models:		'protected/models',
		controllers:	'protected/controllers'
	}
});

// import required files
requirejs(['js/jquery-1.10.2','js/bootstrap.min','controllers/CoinController','models/CoinCalculator', 'views/CoinView', 'views/ErrorView'],

//DOM Load
function  () {
	  $( document ).ready(function() {
		  $('#coinCalcSubmit').on('click', function(){
			  controller= new CoinController();
			  controller.actionView($('#money').val());
			  $('#response').animate({opacity:1},2000, function(){});
			  return false;
		  });
	  });
});

