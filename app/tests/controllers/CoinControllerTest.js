/* 
 * CoinControllerTest
 * 
 * @author Justin Hogg
 */

(function() {

	var obj;

	module("CoinController", {
		setup: function() {
			obj = new CoinController();
		}
	});

	test( "CoinController element test", function() {
	
		equal(obj.element, 'div#response', 'Strings for the element match');
	});
	
})();