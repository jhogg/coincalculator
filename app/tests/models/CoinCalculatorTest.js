/* 
 * CoinCalculatorTest
 * 
 * @author Justin Hogg
 */

(function() {

	var obj;

	module("CoinCalculator", {
		setup: function() {
			obj = new CoinCalculator();
		}
	});

	test( "CoinCalculator object test", 2,function() {
	
		equal(obj.allowedCoins.length, 8, 'allowedCoins index matches the number given');
		
		equal($.isEmptyObject(obj.coins), true, 'coins is an empty object');
	});
	
	test( "CoinCalculator _setCoins test",function() {

		obj._setCoins(123);
		
		equal($.isEmptyObject(obj.coins), false, 'coins is not empty object');
	});
	
	test( "CoinCalculator _coinValue pound test",function() {
		
		val = obj._coinValue(200);
		
		equal(val, '£2', 'coins is equal to a pound coin ');
	});
	
	test( "CoinCalculator _coinValue pence test",function() {
		
		val = obj._coinValue(20);
		
		equal(val, '20p', 'coins is equal to a pence coin ');
	});
	
	test( "CoinCalculator _isInteger valid test",function() {
		
		equal(obj._isInteger('20'), true, 'val is an integer');
	});
	
	test( "CoinCalculator _isInteger not valid test",function() {
		
		equal(obj._isInteger('test'), false, 'val is  non an integer');
	});
	
	test( "CoinCalculator _cleanInput currency £n test",function() {
		
		val = obj._cleanInput('£123');
		
		equal(val, 12300,  'val is equal to the pence amount');
	});
	
	test( "CoinCalculator _cleanInput currency np test",function() {
		
		val = obj._cleanInput('20p');
		
		equal(val, 20, 'val is equal to the pence amount');
	});
	
	test( "CoinCalculator _cleanInput currency £np test",function() {
		
		val = obj._cleanInput('£123.20p');
		
		equal(val, 12320,  'val is equal to the pence amount');
	});
	
	test( "CoinCalculator _cleanInput decimal .n test",function() {
		
		val = obj._cleanInput('.20');
		
		equal(val, 20, 'val is equal to the pence amount');
	});
	
	test( "CoinCalculator _cleanInput decimal n. test",function() {
		
		val = obj._cleanInput('123.');

		equal(val, 12300,  'val is equal to the pence amount');
	});
	
	test( "CoinCalculator getCoins test", 3,function() {
		
		coins = obj.getCoins('123.');

		equal($.isEmptyObject(obj.coins), false, 'coins is not empty object');
		
		equal(obj.coins['£2'], '61', '61 x £2 coins');
		equal(obj.coins['£1'], '1', '1 x £1 coins');
	});
	
})();


