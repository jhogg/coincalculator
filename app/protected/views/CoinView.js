/* 
 * CoinView - view to handle the coin response
 * 
 * @author Justin Hogg
 */

/**
 * CoinView
 * 
 * @param string element - the element to render the view in
 * @param array coins -  coins needed
 */
function CoinView(element, coins){
	
	var output = '';
	output += '<p>The coins you need are:</p>';
	output += '<ul class="inline">';
	//loop through the coins and render the coin and the amount needed
	$.each(coins, function( index, value) {
		output +='<li>'+value+' x <b>'+index+'</b></li>'
	});
	output += '</ul>';
	
	$(element).html(output);
	
}


