/* 
 * ErrorView - view to handle errors
 * 
 * @author Justin Hogg
 */

/**
 * ErrorView
 * 
 * @param string element - the element to render the view in
 * @param mixed options -  data to pass through to the view
 */
function ErrorView(element, options){
	
	$(element).html(options);
	
}


