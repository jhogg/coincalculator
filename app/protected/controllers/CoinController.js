/* 
 * CoinCalculator - controller for the main application
 * 
 * @author Justin Hogg
 */

/**
 * CoinController
 * 
 * @param object coinCalculator
 */
function CoinController(coinCalculator) {
	
	if(coinCalculator == undefined){
		this.coinCalculator = new CoinCalculator();
	} else {
		this.coinCalculator = coinCalculator
	}
	//set the element
	this.element ='div#response';
}

/**
 * actionView - receives input from the view and renders a response.
 * 
 * @param string input - the value of the submitted text field
 */
CoinController.prototype.actionView = function(input) {
	//if we have a input value passed, convert result
	if(input != undefined && input.length > 0){
		//get the coins from the input
		var coins = this.coinCalculator.getCoins(input);
		//render the view
		this.render('CoinView', this.element , coins);
	} else {
		//render the error view
		this.render('ErrorView', this.element ,'The value you entered is not valid! Please add a valid amount of money!');
	}
}

/**
 * render - renders a view based on the parameters passed.
 * 
 * @param string name - the name of the view
 * @param string element - the element to render the view in
 * @param mixed options -  data to pass through to the view
 */
CoinController.prototype.render = function(name,element,options) {
	if ( typeof window[name] === 'function' ) {
		new window[name](element, options);
	}
}