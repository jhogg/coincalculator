/* 
 * CoinCalculator - model
 * 
 * @author - Justin Hogg
 */

/**
 * CoinCalculator
 * 
 */
function CoinCalculator(){
	//set the allowed coins
	this.allowedCoins = [200,100,50,20,10,5,2,1];
	//set the coins being returned
	this.coins = {};
}

/**
 * getCoins - returns the coin object
 * 
 * @param string input - the input value from the form submission
 * @returns object
 */
CoinCalculator.prototype.getCoins= function(input){
	//clean the input
	num = this._cleanInput(input);
	//set the coins
	this._setCoins(num);
	//return the coins needed
	return this.coins;
}

/**
 * _setCoins - adds the number of coins you need for each allowed coin type
 * 
 * @param int num
 */
CoinCalculator.prototype._setCoins = function(num) {
	
	//set the coins allowed
	coins = this.allowedCoins;
	
	//loop over all our coins
	for (var i = 0; i < coins.length; i++){
		//index for the coins object
		var val = coins[i];
		//calculate the max number of times this coin can fit into our input value
		numberOfCoins = Math.floor(num/val);
		//remove the correct number of pennies from our num
		num = num - (numberOfCoins * val);
		//if we have coins then add the amount to the object
		if(numberOfCoins > 0){
			this.coins[this._coinValue(val)] = numberOfCoins;
		}	
	}
}

/**
 * _coinValue - returns a string representaion of the coin needed
 * 
 * @param int val - the value of allowedCoins
 * @returns string
 */
CoinCalculator.prototype._coinValue= function(val){
	
	var decimalValue = Math.pow(10,2);
	
	if(val < decimalValue){
		return val + 'p';
	} else {
		return '£' + (val/decimalValue);
	}
	
}

/**
 * _isInteger - evaluates the input and returns if it is a integer or not
 * 
 * @param string val - input to check against
 * @returns boolean
 */
CoinCalculator.prototype._isInteger = function(val){
	
	//check to see if there are any decimals or other char in here
	var chars  = val.match(/^\d+$/);
	
	//if there are matches then lets exit
	if (chars == undefined || !(chars.length == 1 ) ) {
		return false;
	}
	//if the following are true then it must be a int
	if(Math.floor(val) == val && $.isNumeric(val)){
		return true;
	}
	
	//default retuen false
	return false;
}

/**
 * _cleanInput - cleans the input by stripping all characters and changing the val into pence
 * 
 * @param string val - the input value
 * @returns int
 */
CoinCalculator.prototype._cleanInput = function(val){
	//set the input
	var input = val;
	//if it is only pence remove the currency
	input = input.replace(/p$/g, '');
	
	//if it is an integer now then it is likely we have only pennies
	if(this._isInteger(input)){
		return parseInt(input);
	}
	
	//remove all  other non int from input but leave the decimal if it exists
	input = input.replace(/[^0-9.]/g, '');
	
	if(input.indexOf('.') >= 0){
		//round it up to 2 decimal places
		input = Math.round(input * 100);
		return parseInt(input);
	}

	return input * 100;
}


